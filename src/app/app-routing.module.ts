import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatBackofficeComponent } from './components/chat-backoffice/chat-backoffice.component';
import { LoginComponent } from './components/login/login.component';


const routes: Routes = [
  { path: 'chat', component: ChatBackofficeComponent },
  { path: '', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
