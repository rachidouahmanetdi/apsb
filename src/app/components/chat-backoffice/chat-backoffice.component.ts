import { Component, OnDestroy, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { WebsoketService } from 'src/app/services/websoket.service';
@Component({
  selector: 'app-chat-backoffice',
  templateUrl: './chat-backoffice.component.html',
  styleUrls: ['./chat-backoffice.component.css']
})
export class ChatBackofficeComponent implements OnInit,OnDestroy {
  room: any="23706579b12c4790a8e7858d4e72a2be";
  fullname:any
  msgtext=""
  messageList: any=[];
  userId: string;
  constructor(private chatservice:ChatService,private socket:Socket,private dataService:WebsoketService ) { }
 
  rooms=[]
  token
  ngOnInit() {
    this.fullname=localStorage.getItem("fullname")
    this.userId=localStorage.getItem("userId")
    console.log("userId",this.userId);
    
    // this.initRoom()
    this.getRooms()
    //this.getMessages()
    // this.dataService.currentData.subscribe((message: string) => {
    //   console.log("msg",message);
      
    //   this.messageList.push(message);
    //   console.log("listmsg",this.messageList);
    // });
   
    // this.getMessages().subscribe((message: string) => {
    //   console.log("msg",message);
      
    //   this.messageList.push(message);
    // });
    
    
  }
  getRooms() {
    this.chatservice.getRooms().subscribe((data:any)=>{
      console.log('getList',data);
      this.rooms=data.conversation
      this.rooms.forEach((item)=>{
         this.socket.emit("subscribe",item.chatRoomId);
      })
      
    })
    this.getMessages()

  }

  initRoom() {
    this.chatservice.initRoom().subscribe((data:any)=>{
      console.log("initroom",data);
      this.room=data.chatRoom.chatRoomId


      
    })
  }
  ngOnDestroy(): void {
    this.socket.emit("unsubscribe",{room:this.room});

  }
  checkoutRoom(data){
    
     
      this.room=data.chatRoomId
    
    
  
 }
  sendMsg(msg){
    this.chatservice.postMsg(this.room,msg).subscribe((data:any)=>{
      console.log('postmg',data);
      // this.socket.emit("new_message",{room:roomId,message:msg});

    })
  }
   getMessages = () => {
            
            this.socket.on("new_message", (message) => {
                 this.dataService.updateListMsg(message);
                 console.log("soketmsg",message);
                 this.messageList.push(message)
                 console.log("listMsg",this.messageList);
                 
            });
            
}
  getConversationSenderName(info:any[]){
    let user = info.filter((item)=>{
      return item[0]._id != this.userId
    })
    return user[0][0]["firstName"]+" "+user[0][0]["lastName"]
  }
}
