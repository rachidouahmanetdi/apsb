import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userId = ""
  constructor(
    private chatService: ChatService,
    private formBuilder: FormBuilder,
    private router: Router
    ) {}
  ngOnInit(): void {
  }

  click(): void {
    // Process checkout data here
    this.chatService.getUserById(this.userId).subscribe((data:any)=>{

      localStorage.setItem("fullname",data.user.firstName+" "+data.user.lastName)
      localStorage.setItem("userId",this.userId)
      this.chatService.getLlogin(this.userId).subscribe((data:any)=>{

        localStorage.setItem("token",data.authorization)
        console.warn('Your order has been submitted', this.userId);
        this.router.navigate(['chat']);
  
      });

    });
   
  }

}
