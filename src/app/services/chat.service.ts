import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Local } from 'protractor/built/driverProviders';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  baseUrl: string=environment.baseUrl;
  token=localStorage.getItem("token")
  constructor(private http:HttpClient) { }
  initRoom(){
    
    const obj={
	
      "userIds": ["3e4c8dce2bab46b09f0cec06717bc430"],
      "type": "consumer-to-support"
    }
    console.log(this.token)
    const headers = new HttpHeaders({'Authorization':"Bearer "+this.token});
    
    const options = { headers: headers };
    return this.http.post(this.baseUrl+"room/initiate",obj,options)
  }
  postMsg(roomId,msg){
    
    const obj={messageText:msg}
    const headers = new HttpHeaders({'Authorization':"Bearer "+this.token});
    
    const options = { headers: headers };
    return this.http.post(this.baseUrl+"room/"+roomId+"/message",obj,options)
  }
  getRooms(){
    

    const headers = new HttpHeaders({'Authorization':"Bearer "+this.token});
    
    const options = { headers: headers };
    return this.http.get(this.baseUrl+"room",options)
  }
  getUserById(userId){
    
    return this.http.get(this.baseUrl+"users/"+userId)
  }
  getLlogin(userId){
    
    return this.http.post(this.baseUrl+"login/"+userId,null)
  }
}
