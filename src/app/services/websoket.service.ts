import { EventEmitter, Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class WebsoketService {
  constructor() {}

  private msgSubject = new BehaviorSubject<any>("");
  currentData = this.msgSubject.asObservable();
  updateListMsg(message){
    
    this.msgSubject.next(message)

  }
  
}
